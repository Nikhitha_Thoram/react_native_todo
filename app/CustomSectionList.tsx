import React from 'react';
import {
    Text,
    View,
    SectionList,
    StyleSheet,
  } from "react-native";



  const  CustomSectionList=()=>{
   
    const data = [
      {
        title: 'Main dishes',
        data: ['Pizza', 'Burger', 'Risotto'],
      },
      {
        title: 'Sides',
        data: ['French Fries', 'Onion Rings', 'Fried Shrimps'],
      },
      {
        title: 'Drinks',
        data: ['Water', 'Coke', 'Beer'],
      },
      {
        title: 'Desserts',
        data: ['Cheese Cake', 'Ice Cream'],
      },
    ];
     
    return(
      <View>
        <SectionList
        sections={data}
        keyExtractor={(item,index)=>item+index}
        renderItem={({item})=>(
          <Text>{item}</Text>
        )}
        renderSectionHeader={({section:{title}})=>(
         <View style={styles.header}>
          <Text style={styles.headerText}>{title}</Text>
          </View>
        )}
        />
      </View>
    )


  }
  
  const styles=StyleSheet.create({
  
      header: {
        padding: 10,
        backgroundColor: '#f0f0f0',
    },
    headerText: {
        fontSize: 18,
        fontWeight: 'bold',
    }
    
  })

  export default CustomSectionList;