import React from 'react';
import { View, StyleSheet, Image} from "react-native";
import ImageInput from './ImageInput';

function ImageInputLists({images=[],onRemoveImage,onAddImage}){
    return(
        <View style={styles.container}>
         {images.map(uri=> <ImageInput image={uri} key={uri} onChangeImage={()=>onRemoveImage(uri)}/>) }
         <ImageInput onChangeImage={uri=>onAddImage(uri)}/>
        </View>
    )
}

const styles=StyleSheet.create({
    container:{
     flexDirection:'row',

    }
})


export default ImageInputLists;