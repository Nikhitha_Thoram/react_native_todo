import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';

const Tweets = () => {
  return (
    <View>
      <Text>Tweets</Text>
    </View>
  );
};

const TweetDetails = () => {
  return (
    <View>
      <Text>Tweet Details Screen</Text>
    </View>
  );
};

const Stack = createStackNavigator();
const StackNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen name='Tweets' component={Tweets}/>
    <Stack.Screen name='TweetDetails' component={TweetDetails}/>
  </Stack.Navigator>
);

function NavigationComponent() {
  return (
    
      <StackNavigator />
  );
}

export default NavigationComponent;
