import {
  Text,
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { useState ,useRef} from "react";
import Task from "@/components/Task";

export default function Index() {
  const [task, setTask] = useState("");
  const [taskItems, setTaskItems] = useState([]);

  const handleAddTask = () => {
    setTaskItems([...taskItems, task]);
    setTask("");

  };
  
  const handleDelete=(index)=>{
    const newItems = [...taskItems];
    newItems.splice(index,1);
    setTaskItems(newItems);
  }
  return (
    <View style={styles.conatiner}>
      <View style={styles.wrapper}>
        <Text style={styles.title}>Today's Tasks</Text>
        <ScrollView style={styles.scrollView} contentContainerStyle={styles.scrollViewContent}>
        <View style={styles.taskItem}>
          {taskItems.map((item, index) => (
            <Task key={index} text={item} onDelete={()=>handleDelete(index)}/>
          ))}
        </View>
        </ScrollView>
      </View>
      
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={styles.addTaskWrapper}
      >
        <TextInput
          style={styles.input}
          value={task}
          placeholder="Add a new task.."
          onChangeText={(text) => setTask(text)}
        />
        <TouchableOpacity onPress={() => handleAddTask()}>
          <View style={styles.addContainer}>
            <Text style={styles.addIcon}>+</Text>
          </View>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    </View>
  );
}


const styles = StyleSheet.create({
  conatiner: {
    flex: 1,
    backgroundColor: "#E8EAED",
  },
  taskItem: {},
  wrapper: {
    paddingTop: 80,
    paddingHorizontal: 20,
    flex:1,

  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
  },
  addTaskWrapper: {
    position: "absolute",
    bottom: 20,
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingHorizontal: 20,
    paddingBottom: Platform.OS === "ios" ? 20 : 0,
  },
  input: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    backgroundColor: "#FFF",
    borderRadius: 60,
    borderColor: "#C0C0C0",
    borderWidth: 1,
    width: 220,
  },

  addContainer: {
    width: 50,
    height: 50,
    backgroundColor: "#FFF",
    borderRadius: 50,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 1,
    borderColor: "#C0C0C0",
  },
  addIcon: {
    fontSize: 14,
  },
  scrollView: {
    flex: 1,
  },
  scrollViewContent: {
    paddingHorizontal: 20,
    paddingBottom:100
   },
});
