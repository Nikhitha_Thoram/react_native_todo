import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Platform } from "react-native";
import * as ImagePicker from 'expo-image-picker';
import ImageInput from './ImageInput';

function ImagePickerComponent() {
    const [image, setImage] = useState();

    useEffect(() => {
        requestPermission();
    }, []);

    const requestPermission = async () => {
        if (Platform.OS !== 'web') {
            const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
            if (status !== 'granted') {
                alert('Sorry, we need camera roll permissions to make this work!');
            }
        }
    };

    const selectImage = async () => {
        try {
            const result = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                quality: 0.5,
            });
            if (!result.canceled) {
                setImage(result.assets[0].uri);
            }
        } catch (error) {
            console.error('Error reading an image', error);
        }
    };

    const handleChangeImage = (uri) => {
        if (uri === null) {
            setImage(null);
        } else {
            selectImage();
        }
    };

    return (
        <View style={styles.container}>
            <ImageInput image={image} onChangeImage={handleChangeImage} />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default ImagePickerComponent;