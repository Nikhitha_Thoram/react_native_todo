import React from 'react';
import { View, StyleSheet, Image, TouchableWithoutFeedback, Alert } from "react-native";
import { MaterialCommunityIcons } from '@expo/vector-icons';

function ImageInput({ image, onChangeImage }) {
    const handlePress = () => {
        if (!image) {
            onChangeImage();
        } else {
            Alert.alert('Delete', 'Are you sure you want to delete this image?', [
                { text: 'Yes', onPress: () => onChangeImage(null) },
                { text: 'No' },
            ]);
        }
    };

    return (
        <TouchableWithoutFeedback onPress={handlePress}>
            <View style={styles.container}>
                {!image && <MaterialCommunityIcons name="camera" size={40} color="#fff" />}
                {image && <Image source={{ uri: image }} style={styles.image} />}
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#e3e3e3',
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
        width: 100,
        overflow: 'hidden',
    },
    image: {
        width: '100%',
        height: '100%',
    },
});

export default ImageInput;