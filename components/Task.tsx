import React from "react";
import { Text, View, StyleSheet ,TouchableOpacity} from "react-native";
import { Swipeable } from "react-native-gesture-handler";
import Icon from "react-native-vector-icons/MaterialIcons";

const Task = ({ text, onDelete }) => {
    const renderRight = () => {
      return (
        <TouchableOpacity onPress={onDelete} style={styles.deleteButton}>
          <Icon name='delete' size={24} color="black" />
        </TouchableOpacity>
      );
    }; 
  
    return (
      <Swipeable renderRightActions={renderRight}>
        <View style={styles.item}>
          <View style={styles.leftItem}>
            <View style={styles.square}></View>
            <Text style={styles.text}>{text}</Text>
          </View>
        </View>
      </Swipeable>
    );
  };

const styles = StyleSheet.create({
    item: {
        backgroundColor: '#FFF',
        padding: 15,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 10,
        marginBottom: 4

    },
    leftItem: {
        flexDirection: 'row',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    square: {
        width: 24,
        height: 24,
        opacity: 0.4,
        backgroundColor: '#55BCFC',
        borderRadius: 5,
        marginRight: 15
    },
    text: {
        fontSize: 12,

    },
    deleteButton:{
        justifyContent: 'center',
        alignItems: 'center',
        margin:3,
    }

})

export default Task;














// import React from "react";
// import { View, Text, StyleSheet, TextInput, Button } from "react-native";
// import { Formik } from "formik";
// import * as Yup from "yup";

// const validationSchema = Yup.object().shape({
//   email: Yup.string().email("Invalid email").required("Email is required"),
//   password: Yup.string()
//     .min(6, "Password must be at least 6 characters")
//     .required("Password is required"),
//   confirmPassword: Yup.string()
//     .oneOf([Yup.ref("password"), null], "Passwords must match")
//     .required("Confirm Password is required"),
// });

// function RegisterScreen({ navigation }) {
//   const handleSubmit = (values) => {
//     console.log("Register pressed with:", values);
//   };

//   return (
//     <View style={styles.container}>
//       <Text style={styles.title}>Register</Text>
//       <Formik
//         initialValues={{ email: "", password: "", confirmPassword: "" }}
//         validationSchema={validationSchema}
//         onSubmit={handleSubmit}
//       >
//         {({ handleChange, handleBlur, handleSubmit, values, errors, touched }) => (
//           <>
//             <TextInput
//               style={styles.input}
//               placeholder="Email"
//               keyboardType="email-address"
//               onChangeText={handleChange("email")}
//               onBlur={handleBlur("email")}
//               value={values.email}
//             />
//             {touched.email && errors.email && (
//               <Text style={styles.errorText}>{errors.email}</Text>
//             )}

//             <TextInput
//               style={styles.input}
//               placeholder="Password"
//               secureTextEntry
//               onChangeText={handleChange("password")}
//               onBlur={handleBlur("password")}
//               value={values.password}
//             />
//             {touched.password && errors.password && (
//               <Text style={styles.errorText}>{errors.password}</Text>
//             )}

//             <TextInput
//               style={styles.input}
//               placeholder="Confirm Password"
//               secureTextEntry
//               onChangeText={handleChange("confirmPassword")}
//               onBlur={handleBlur("confirmPassword")}
//               value={values.confirmPassword}
//             />
//             {touched.confirmPassword && errors.confirmPassword && (
//               <Text style={styles.errorText}>{errors.confirmPassword}</Text>
//             )}

//             <Button
//               title="Register"
//               color="tomato"
//               onPress={handleSubmit}
//             />
//           </>
//         )}
//       </Formik>
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   title: {
//     fontSize: 24,
//     marginBottom: 20,
//   },
//   input: {
//     width: "80%",
//     height: 40,
//     borderColor: "gray",
//     borderWidth: 1,
//     marginBottom: 10,
//     borderRadius: 40,
//     paddingHorizontal: 10,
//   },
//   errorText: {
//     color: "red",
//     fontSize: 12,
//     marginBottom: 5,
//   },
// });

// export default RegisterScreen;